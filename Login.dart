import 'package:flutter/material.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(
            width: 200,
            child: TextField(
              decoration: InputDecoration(label: Text("username")),
            ),
          ),
          const SizedBox(
            width: 200,
            child: TextField(
              decoration: InputDecoration(label: Text("password")),
            ),
          ),
          const SizedBox(
            height: 18,
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(primary: Colors.red),
            onPressed: (() => Navigator.pushNamed(context, "/home")),
            child: const Text("Login"),
          ),
          const Text("Fogot password?"),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Don't have an account?"),
                InkWell(
                  child: const Text(
                    " register",
                    style: TextStyle(color: Colors.red),
                  ),
                  onTap: () => {Navigator.pushNamed(context, "/register")},
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
