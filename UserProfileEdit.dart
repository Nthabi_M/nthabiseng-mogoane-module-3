import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink,
        title: const Text("UserProfileEdit"),
      ),
      body: ListView(
        children: [
          const SizedBox(
            height: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 255,
                child: TextFormField(
                  decoration: const InputDecoration(label: Text("username")),
                  initialValue: "Site",
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  decoration: const InputDecoration(
                    label: Text("name"),
                    enabled: false,
                  ),
                  initialValue: "Nthabi",
                  enabled: false,
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  decoration: const InputDecoration(
                    label: Text("surname"),
                  ),
                  initialValue: "Mogoane",
                  enabled: false,
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  decoration: const InputDecoration(label: Text("email")),
                  initialValue: "allFor1@webmail.com",
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  obscureText: true,
                  decoration: const InputDecoration(label: Text("password")),
                  initialValue: "3214",
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  obscureText: true,
                  decoration:
                      const InputDecoration(label: Text("confirm password")),
                  initialValue: "3214",
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(primary: Colors.pink),
                onPressed: (() => Navigator.pushNamed(context, "/")),
                child: const Text("confirm"),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
