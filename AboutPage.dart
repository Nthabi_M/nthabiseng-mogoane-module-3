import 'package:flutter/material.dart';

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("About"),
        backgroundColor: Colors.pink,
      ),
      body: const Center(
          child: Text(
        "I Love MTN App Academy",
        style: TextStyle(color: Color.fromARGB(255, 0, 160, 255), fontSize: 20),
      )),
    );
  }
}
