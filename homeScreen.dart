import 'package:flutter/material.dart';

class MyHome extends StatelessWidget {
  const MyHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Dashboard"),
        backgroundColor: Colors.yellow,
      ),
      body: Center(
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          ElevatedButton(
            onPressed: () => {Navigator.pushNamed(context, "/about")},
            style: ElevatedButton.styleFrom(primary: Colors.yellow),
            child: const Text("About"),
          ),
          ElevatedButton(
            onPressed: () => {Navigator.pushNamed(context, "/year")},
            style: ElevatedButton.styleFrom(primary: Colors.yellow),
            child: const Text("Year"),
          )
        ]),
      ),
      floatingActionButton: ElevatedButton(
        style: ElevatedButton.styleFrom(primary: Colors.yellow),
        child: const Icon(Icons.person),
        onPressed: () => Navigator.pushNamed(context, "/profile"),
      ),
    );
  }
}
